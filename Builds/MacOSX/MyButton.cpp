//
//  MyButton.cpp
//  JuceBasicWindow
//
//  Created by Andrew Warren on 28/10/2015.
//
//

#include "MyButton.h"

MyButton::MyButton()
{
    setSize(50, 50);
}

MyButton::~MyButton()
{
    
}

void MyButton::resized()
{
    
}

void MyButton::paint (Graphics& g)
{
    if (isMouseButtonDown())
    {
        g.fillAll(Colours::red);
    }
    
    else if (isMouseOver())
    {
        g.fillAll(Colours::grey);
    }
    else
    {
        g.fillAll(Colours::black);
    }
    
    
}

void MyButton::mouseEnter(const MouseEvent & event )
{
    DBG("MOUSE IS OVER BUTTON\n");
    
    x = event.x;
    y = event.y;
    
    repaint();
}

void MyButton::mouseExit(const MouseEvent & event)
{
    DBG("MOUSE HAS EXITED BUTTON\n");
    x = event.x;
    y = event.y;
    
    repaint();
}


void MyButton::mouseDown(const MouseEvent & event )
{
    DBG("BUTTON IS PRESSED IS PRESSED\n");
    
    x = event.x;
    y = event.y;
    
    repaint();
}

void MyButton::mouseUp(const MouseEvent & event)
{
    DBG("BUTTON IS RELEASED\n");
    
    x = event.x;
    y = event.y;
    
    repaint();
}

