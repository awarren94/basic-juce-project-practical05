//
//  MyButton.h
//  JuceBasicWindow
//
//  Created by Andrew Warren on 28/10/2015.
//
//

#ifndef MYBUTTON_H_INCLUDED
#define MYBUTTON_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"

class MyButton  : public Component
{
public:
    MyButton();
    ~MyButton();
    
    void resized() override;
    void paint(Graphics& g) override;
    void mouseEnter(const MouseEvent & );
    void mouseExit(const MouseEvent & );
    void mouseDown(const MouseEvent & );
    void mouseUp(const MouseEvent & );
    
    
private:
    int x;
    int y;
};

#endif /* MYBUTTON_H_INCLUDED */
