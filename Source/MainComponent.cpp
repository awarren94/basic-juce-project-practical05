/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    
    //addAndMakeVisible(colourSelector1);
    addAndMakeVisible(myButton1);
    
}

MainComponent::~MainComponent()
{
    
}

void MainComponent::resized()
{
    DBG("Height: " << getHeight() << "\n");
    DBG("Width: " << getWidth() << "\n");
    myButton1.setBounds(0, 0, getWidth()/2, getHeight()/2);
    colourSelector1.setBounds(0, 0 , getWidth(), getHeight()/2);
}


void MainComponent::mouseUp(const MouseEvent & event)
{
    DBG("MOUSE IS RELEASED");
    
    /**X and Y declared as private member variables*/
    x = event.x;
    y = event.y;

    /**Print coordinates of mouse when clicked*/
    DBG("x " << event.x << ":" << " y " << event.y);
    
    /**Calls paint function by marking it as needing to be redrawn*/
    repaint();
}

void MainComponent::mouseDown(const MouseEvent & event )
{
    DBG("MOUSE IS PRESSED");
    
    x = event.x;
    y = event.y;
    
    
    
    repaint();
}


void MainComponent::mouseExit(const MouseEvent & event )
{
    DBG("MOUSE IS PRESSED");
    x = event.x;
    y = event.y;
    
    repaint();
}

void MainComponent::paint (Graphics& g)
{
    DBG("PAINT FUNCTION IS CALLED ");
   
    DBG(x << ": " << y);
    
    /**Use colour selector to set colour of mouse click*/
    g.setColour(colourSelector1.getCurrentColour());
    
    /** draws ellipse when mouse is clicked */
    g.fillEllipse(x - 6, y - 7, 10, 10);
    
}
