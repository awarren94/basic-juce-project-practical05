/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#ifndef MAINCOMPONENT_H_INCLUDED
#define MAINCOMPONENT_H_INCLUDED

#include "../JuceLibraryCode/JuceHeader.h"
#include "MyButton.h"

//==============================================================================
/*
    This component lives inside our window, and this is where you should put all
    your controls and content.
*/
class MainComponent   : public Component
{
public:
    //==============================================================================
    MainComponent();
    ~MainComponent();

    void resized() override;
    void mouseUp ( const MouseEvent & );
    void mouseDown ( const MouseEvent & );
   // void mouseEnter (const MouseEvent & );
    void mouseExit (const MouseEvent & );
    
    void paint (Graphics& g) override;
    
    
    
  
    

private:
    //==============================================================================
    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (MainComponent)
    
    /**variables to stores mouse position*/
    int x;
    int y;
    
    ColourSelector colourSelector1;
    MyButton myButton1;
};


#endif  // MAINCOMPONENT_H_INCLUDED
